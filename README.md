# Introduction
Converts a string into an expression and tries to evalute the condition. Uses simple Math Rules.
Math-signs: '=', '!', '<', '>', '+', '-', '*', '/', '^', '()'
Run it like this:
```csharp
var parser = new ConditionParser(new VoidResolver());
var result = parser.EvaluateCondition("1+1*(1+1)");
```

# Installation

Install the following nuget-package: String.Math

```csharp
Install-Package String.Math
```

## Usage

Import the reference in your file
```csharp
using String.Math;
```

Example

```csharp
using System;
using String.Math;

namespace test;

public class TestClass
{
    public void TestParser()
    {
        string dataToTest = "1+2(3+4)*8";
        
        var parser = new ConditionParser(new VoidResolver());
        
        var result = parser.EvaluateCondition(dataToTest);
        
        Console.Out.WriteLine(result);
    }
}
```

## Supports

The following operators and constants can be used 
```
+
-
/
*
^
sin
cos
tan
arcsin
arccos
arctan
()
=
!
<
>

pi
e
```
