using System.Collections.Generic;
using Xunit;

namespace String.Math.Tests;

public class ConditionParserTest
{
    [Theory]
    [InlineData("1+(3+5)/4*5", "11")]
    [InlineData("2(2+8)+1", "21")]
    [InlineData("4+(1+(2*3)/2+3)(4*2)", "60")]
    [InlineData("4^2*2", "32")]
    [InlineData("6/3*2", "4")]
    [InlineData("@12+5", "17")]
    [InlineData("((((((((8))))))))", "8")]
    [InlineData("(6)(4+(2*3))", "60")]
    public void TestMathStrings(string input, string expected)
    {
        var parser = new ConditionParser(new VoidResolver());

        Assert.Equal(expected, parser.EvaluateCondition(input));
    }
    
    [Theory]
    [InlineData("4*@Three", "12")]
    [InlineData("1+3*(@Three)", "10")]
    [InlineData("@OnePlusThree", "4")]
    public void TestMathStringsWithResolverDefaultSettings(string input, string expected)
    {
        var dictionary = new Dictionary<string, string>
        {
            {"Three", "3"},
            {"OnePlusThree", "4"},
            {"NotANumber", "q"}
        };

        var resolver = new DictionaryResolver(dictionary);
        
        var parser = new ConditionParser(resolver);

        Assert.Equal(expected, parser.EvaluateCondition(input));
    }
    
    [Theory]
    [InlineData("@NotANumber", "1")]
    public void TestReturn1(string input, string expected)
    {
        var dictionary = new Dictionary<string, string>
        {
            {"Three", "3"},
            {"OnePlusThree", "1+3"},
            {"NotANumber", "q"}
        };

        var resolver = new DictionaryResolver(dictionary);

        var parser = new ConditionParser(resolver);

        parser.ParserSetting.HandleFailedDataResolve = OnFailureEnum.Return1;

        Assert.Equal(expected, parser.EvaluateCondition(input));
    }

    [Theory]
    [InlineData("@NotANumber", "0")]
    public void TestReturn0(string input, string expected)
    {
        var dictionary = new Dictionary<string, string>
        {
            {"Three", "3"},
            {"OnePlusThree", "1+3"},
            {"NotANumber", "q"}
        };

        var resolver = new DictionaryResolver(dictionary);
        
        ParserSetting parserSettings = new ()
        {
            HandleFailedDataResolve = OnFailureEnum.Return0
        };
        
        var parser = new ConditionParser(resolver, parserSettings);

        Assert.Equal(expected, parser.EvaluateCondition(input));
    }
    
    [Theory]
    [InlineData("@NotANumber*2", "8")]
    [InlineData("@RecursiveData*2", "8")]
    public void TestRecursiveParsing(string input, string expected)
    {
        var dictionary = new Dictionary<string, string>
        {
            {"RecursiveData", "@NotANumber*1"},
            {"OnePlusThree", "1+3"},
            {"NotANumber", "@OnePlusThree"}
        };

        var resolver = new DictionaryResolver(dictionary);

        ParserSetting parserSettings = new ()
        {
            ShouldDataResolveBeParsed = ParseValueEnum.RecursiveParser
        };
        
        var parser = new ConditionParser(resolver, parserSettings);

        Assert.Equal(expected, parser.EvaluateCondition(input));
    }
    
    [Theory]
    [InlineData("5+sin(90)", "6", AngleSystemEnum.Degrees)]
    [InlineData("5*sin(90)", "5", AngleSystemEnum.Degrees)]
    [InlineData("5+9*sin(90)", "14", AngleSystemEnum.Degrees)]
    [InlineData("5+3sin(270)", "2", AngleSystemEnum.Degrees)]
    [InlineData("5+cos(0)", "6", AngleSystemEnum.Degrees)]
    [InlineData("5+3cos(180)", "2", AngleSystemEnum.Degrees)]
    [InlineData("5+3arcsin(0)", "5", AngleSystemEnum.Degrees)]
    [InlineData("5+3arccos(0)", "275", AngleSystemEnum.Degrees)]
    [InlineData("10*sin(arcsin(1))", "10", AngleSystemEnum.Degrees)]
    [InlineData("10sin(arcsin(1))=10", "1", AngleSystemEnum.Degrees)]
    [InlineData("10sin(pi)=0", "1", AngleSystemEnum.Radians)]
    [InlineData("5cos(pi)", "-5", AngleSystemEnum.Radians)]
    public void TestAngleMathStrings(string input, string expected, AngleSystemEnum angleSystem)
    {
        ConditionParser parser = new (new VoidResolver(), new ParserSetting()
        {
            AngleSystem = angleSystem
        });

        Assert.Equal(expected, parser.EvaluateCondition(input));
    }

    [Theory]
    [InlineData("1", true)]
    [InlineData("2", true)]
    [InlineData("3", true)]
    [InlineData("4", true)]
    [InlineData("5", true)]
    [InlineData("6", true)]
    [InlineData("7", true)]
    [InlineData("8", true)]
    [InlineData("9", true)]
    [InlineData("0", true)]
    [InlineData("/", false)]
    [InlineData("+", false)]
    public void IsNotaSignTest(string input, bool expected)
    {
        Assert.Equal(expected, ConditionParser.IsNotASign(input));
    }
}