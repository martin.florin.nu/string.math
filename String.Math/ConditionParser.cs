﻿using System.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace String.Math;

public class ConditionParser
{
	private readonly IDataResolver _dataResolver;
		
	public ParserSetting ParserSetting { get; }
		
	//The order the program calculates the different operators
	//It's a reversed order.
	private static readonly Regex RSum = new(@"=", RegexOptions.Compiled); 
	private static readonly Regex RGreaterThan = new(@">", RegexOptions.Compiled); 
	private static readonly Regex RLesserThan = new(@"<", RegexOptions.Compiled); 
	private static readonly Regex RNot = new(@"!", RegexOptions.Compiled); 
	private static readonly Regex RAdd = new(@"\+", RegexOptions.Compiled); 
	private static readonly Regex RSubtract = new(@"\-", RegexOptions.Compiled); 
	private static readonly Regex RMulti = new(@"\*", RegexOptions.Compiled); 
	private static readonly Regex RDivide = new(@"\/", RegexOptions.Compiled); 
	private static readonly Regex RExp = new(@"\^", RegexOptions.Compiled); 
	private static readonly Regex RSin = new(@"sin", RegexOptions.Compiled); 
	private static readonly Regex RCos = new(@"cos", RegexOptions.Compiled); 
	private static readonly Regex RTan = new(@"tan", RegexOptions.Compiled); 
	private static readonly Regex RArcsin = new(@"arcsin", RegexOptions.Compiled); 
	private static readonly Regex RArccos = new(@"arccos", RegexOptions.Compiled); 
	private static readonly Regex RArctan = new(@"arctan", RegexOptions.Compiled); 
	
	private static readonly char[] SignsInOrder = {'=', '>', '<', '!', '+', '-', '*', '/', '^'};
	private static readonly Dictionary<string, Regex> RegexSignsInOrder =
		new()
		{
			{"=", RSum},
			{">", RGreaterThan},
			{"<", RLesserThan},
			{"!", RNot},
			{"+", RAdd},
			{"-", RSubtract},
			{"*", RMulti},
			{"/", RDivide},
			{"^", RExp},
			{"arcsin", RArcsin},
			{"arccos", RArccos},
			{"arctan", RArctan},
			{"sin", RSin},
			{"cos", RCos},
			{"tan", RTan},
		};

	private static readonly Dictionary<string, double> Constants =
		new()
		{
			{"pi", System.Math.PI},
			{"e", System.Math.E},
		};

	public ConditionParser(IDataResolver dataResolver, double precision = 1e-06)
	{
		_dataResolver = dataResolver ?? throw new ArgumentNullException(nameof(dataResolver));
		ParserSetting = new ParserSetting()
		{
			Precision = precision
		};
	}
		
	public ConditionParser(IDataResolver dataResolver, ParserSetting parserSetting)
	{
		_dataResolver = dataResolver ?? throw new ArgumentNullException(nameof(dataResolver));
		ParserSetting = parserSetting;
	}

	/// <summary>
	/// Evaluates the string and make it into a value
	/// </summary>
	/// <param name="mathString">the string to evaluate</param>
	/// <returns>the calculated value</returns>
	public string EvaluateCondition(string mathString)
	{
		var result = CalculateWithParenthesisRecursive(FormatString(mathString));
		return result;
	}

	private static string FormatString(string input)
	{
		var result = input.Replace(")(", ")*(");
		for (int i = 0; i < 9; i++)
		{
			result = result.Replace($"{i}(", $"{i}*(").Replace($"){i}", $")*{i}");
			result = result.Replace($"{i}tan(", $"{i}*tan(");
			result = result.Replace($"{i}sin(", $"{i}*sin(");
			result = result.Replace($"{i}cos(", $"{i}*cos(");
			result = result.Replace($"{i}arctan(", $"{i}*arctan(");
			result = result.Replace($"{i}arcsin(", $"{i}*arcsin(");
			result = result.Replace($"{i}arccos(", $"{i}*arccos(");
		}

		// foreach (var replacer in Replacers)
		// {
		// 	result = Regex.Replace( input, $"[{allSigns}]{replacer.Key}", $"*1{replacer.Key}").Replace(replacer.Key, replacer.Value);	
		// }
		
		return result;
	}

	/// <summary>
	/// Split the string with parenthesis. When no parenthesis are left it send the value to the calculate function.
	/// Recursive function
	/// </summary>
	/// <param name="input">the incoming string</param>
	/// <returns></returns>
	private string CalculateWithParenthesisRecursive(string input)
	{
		//Check if we have any parenthesis left
		if (input.IndexOf("(", StringComparison.Ordinal) == -1 && IsNotASign(input))
		{
			return CalculateWithoutParenthesisRecursive(input, 0);
		}

		if (input.IndexOf("(", StringComparison.Ordinal) == -1)
		{
			return input;
		}

		//pick out the first parenthesis and run calculate on everything inside the parenthesis and everything after.
		string begin = input.Substring(0, input.IndexOf("(", StringComparison.Ordinal));
		string mid = InsideParenthesis(input.Substring(input.IndexOf("(", StringComparison.Ordinal)));
		string end = AfterParenthesis(input.Substring(input.IndexOf("(", StringComparison.Ordinal)));
		return CalculateWithParenthesisRecursive(begin + CalculateWithParenthesisRecursive(mid) +
		                                         CalculateWithParenthesisRecursive(end));
	}

	public static bool IsNotASign(string input)
	{
		foreach (var sign in SignsInOrder)
		{
			if (input.IndexOf(sign) == 0)
			{
				return false;
			}
		}

		return true;
	}

	private string CalculateWithoutParenthesisRecursive(string input, int signIndex)
	{
		if (string.IsNullOrWhiteSpace(input))
		{
			return input;
		}

		if (double.TryParse(input, out var onlyANumber))
		{
			return onlyANumber.ToString(CultureInfo.InvariantCulture);
		}

		var sign = RegexSignsInOrder.ElementAt(signIndex);

		string[] parts = sign.Value.Split(input);
		double sum = (signIndex >= RegexSignsInOrder.Count - 1
			? GetValue(parts[0])
			: GetValue(CalculateWithoutParenthesisRecursive(parts[0], signIndex + 1)));
		for (int i = 1; i < parts.Length; i++)
		{
			sum = EvaluateMathSign(sum,
				(signIndex >= RegexSignsInOrder.Count - 1
					? GetValue(parts[i])
					: GetValue(CalculateWithoutParenthesisRecursive(parts[i], signIndex + 1))), signIndex);
		}

		string result = sum.ToString(CultureInfo.InvariantCulture);

		return result;
	}

	private double EvaluateMathSign(double first, double second, int sign)
	{
		switch (RegexSignsInOrder.ElementAt(sign).Key)
		{
			case "tan":
				return System.Math.Tan(ToRadians(second));
			case "sin":
				return System.Math.Sin(ToRadians(second));
			case "cos":
				return System.Math.Cos(ToRadians(second));
			case "arctan":
				return ToDegrees(System.Math.Atan(second));
			case "arcsin":
				return ToDegrees(System.Math.Asin(second));
			case "arccos":
				return ToDegrees(System.Math.Acos(second));
			case "^":
				return System.Math.Pow(first, second);
			case "/":
				return first / second;
			case "*":
				return first * second;
			case "-":
				return first - second;
			case "+":
				return first + second;
			case "!":
				return (System.Math.Abs(first - second) > ParserSetting.Precision ? 1.0 : 0.0);
			case "=":
				return (System.Math.Abs(first - second) < ParserSetting.Precision ? 1.0 : 0.0);
			case ">":
				return (first > second ? 1.0 : 0.0);
			case "<":
				return (first < second ? 1.0 : 0.0);
			default:
				return 0.0;
		}
	}

	private double GetValue(string parser)
	{
		try
		{
			if (string.IsNullOrWhiteSpace(parser))
			{
				return 0;
			}
			
			if (Constants.TryGetValue(parser, out var constantValue))
			{
				return constantValue;
			}
			
			if (parser.IndexOf("@", StringComparison.Ordinal) == -1)
			{
				return double.Parse(parser, NumberStyles.Any, CultureInfo.InvariantCulture);
			}
			
			var name = parser.Substring(parser.IndexOf("@", StringComparison.Ordinal) + 1);
			var value = _dataResolver.Resolve($"{name}");

			return ParserSetting.ShouldDataResolveBeParsed switch
			{
				ParseValueEnum.DoNothing => System.Math.Round(double.Parse(value), 6),
				ParseValueEnum.RecursiveParser => Double.Parse(EvaluateCondition(value)),
				_ => throw new ArgumentOutOfRangeException()
			};
		}
		catch (FormatException nfe)
		{
			if (parser == "")
			{
				return 0;
			}

			return ParserSetting.HandleFailedDataResolve switch
			{
				OnFailureEnum.ThrowError => throw new FormatException("Not a number!", nfe),
				OnFailureEnum.Return1 => 1,
				OnFailureEnum.Return0 => 0,
				_ => throw new ArgumentOutOfRangeException()
			};
		}
	}

	private static string InsideParenthesis(string cur)
	{
		var counter = 0;
		for (int i = 0; i < cur.Length; i++)
		{
			if (cur[i] == '(')
			{
				counter++;
			}
			else if (cur[i] == ')')
			{
				counter--;
			}

			if (counter == 0)
			{
				return cur.Substring(1, i - 1);
			}
		}

		return cur;
	}

	private static string AfterParenthesis(string cur)
	{
		var counter = 0;
		for (var i = 0; i < cur.Length; i++)
		{
			if (cur[i] == '(')
			{
				counter++;
			}
			else if (cur[i] == ')')
			{
				counter--;
			}

			if (counter == 0)
			{
				return cur.Substring(i + 1);
			}
		}

		return cur;
	}

	private double ToRadians(double value)
	{
		return ParserSetting.AngleSystem switch
		{
			AngleSystemEnum.Degrees => value * System.Math.PI / 180,
			_ => value
		};
	}
	
	private double ToDegrees(double value)
	{
		return ParserSetting.AngleSystem switch
		{
			AngleSystemEnum.Degrees => value * 180 / System.Math.PI,
			_ => value
		};
	}
}
