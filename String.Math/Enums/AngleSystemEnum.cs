namespace String.Math;

public enum AngleSystemEnum
{
    Degrees = 0,
    Radians
}