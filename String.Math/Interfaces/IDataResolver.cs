namespace String.Math
{
    /// <summary>
    /// Resolver for getting data into the ConditionParser
    /// </summary>
    public interface IDataResolver
    {
        string Resolve(string key);
    }
}