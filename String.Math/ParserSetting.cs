namespace String.Math;

public record ParserSetting
{
    public double Precision { get; set; } = 1e-06;

    public OnFailureEnum HandleFailedDataResolve { get; set; } = OnFailureEnum.ThrowError;

    public ParseValueEnum ShouldDataResolveBeParsed { get; set; } = ParseValueEnum.DoNothing;
    
    public AngleSystemEnum AngleSystem { get; set; }
}