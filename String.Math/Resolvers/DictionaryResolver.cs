using System;
using System.Collections.Generic;

namespace String.Math;

public class DictionaryResolver : IDataResolver
{
    private Dictionary<string, string> _dictionary;

    public DictionaryResolver(Dictionary<string, string> dictionary)
    {
        _dictionary = dictionary ?? throw new ArgumentNullException(nameof(dictionary));
    }
    
    public string Resolve(string key)
    {
        if (string.IsNullOrWhiteSpace(key))
        {
            return null;
        }

        if (_dictionary.ContainsKey(key) == false)
        {
            return null;
        }

        return _dictionary[key];
    }
}