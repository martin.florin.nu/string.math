namespace String.Math
{

    /// <summary>
    /// Returns key
    /// </summary>
    public class VoidResolver : IDataResolver
    {
        public string Resolve(string key)
        {
            return key;
        }
    }
}